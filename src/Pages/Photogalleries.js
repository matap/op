import React, { Component } from 'react';
import PhotogalleriesComponent from '../Components/Photogalleries';


class Photogalleries extends Component {
  render() {
    return (
      <div className="main-content">
        <div className="op-container animatedParent animateOnce">
          <div className="row">
            <div className="col-xs-12">
              <div style={{ "padding": "0 15px"}}>
                <h1 className="main-title">
                  Fotogalerie
                </h1>
              </div>
            </div>
          </div>
        </div>
        <PhotogalleriesComponent />
      </div>
    );
  }
} 

export default Photogalleries;