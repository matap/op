import React from 'react';
import { translate } from 'react-translate';
import Textblock from '../Components/Textblock';

const MassInfo = (props) => {
  return (
    <div className="mass-info">
        <strong>Modlitby:</strong><br />
        <Textblock codename="modlitby" language={props.language} />
    </div>
  )
}

export default translate("PrayerInfo")(MassInfo);