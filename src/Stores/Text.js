import Client from "../Client.js";
//import { SortOrder } from 'kentico-cloud-delivery-typescript-sdk';
import { initLanguageCodeObject, defaultLanguage } from '../Utilities/LanguageCodes'

let textList = initLanguageCodeObject();
let textDetails = initLanguageCodeObject();

let changeListeners = [];

let notifyChange = () => {
  changeListeners.forEach((listener) => {
    listener();
  });
}
class TextStore {
  // Actions
  provideText(textSlug, language) {
    let query = Client.items()
      .type('text')
      .equalsFilter('elements.urlpattern', textSlug);
      //.elementsParameter(['title', 'teaser_image', 'post_date', 'body_copy', 'video_host', 'video_id', 'tweet_link', 'theme', 'display_options']);

    if (language) {
        query.languageParameter(language);
    }
    query.getObservable()
      .subscribe(response => {
        if (!response.isEmpty) {
          if (language) {
            textDetails[language][textSlug] = response.items[0];
          } else {
            textDetails[defaultLanguage][textSlug] = response.items[0];
          }
          notifyChange();
        }
      })
  }

  provideTexts(count, language) {
    let query = Client.items()
      .type('text')
      //.orderParameter("elements.post_date", SortOrder.desc);

    // if (language) {
    //   query.languageParameter(language);
    // }

    query.getObservable()
      .subscribe(response => {
        if (language) {
            textList[language] = response.items;
        } else {
            textList[defaultLanguage] = response.items
        }
        notifyChange();
      });
  }

  // Methods
  getText(textSlug, language) {
    if (language) {
      return textDetails[language][textSlug];
    } else {
      return textDetails[defaultLanguage][textSlug];
    }
  }

  getTexts(count, language) {
    if (language) {
      return textList[language].slice(0, count);
    }
    else {
      return textList[defaultLanguage].slice(0, count);
    }
  }

  // Listeners
  addChangeListener(listener) {
    changeListeners.push(listener);
  }

  removeChangeListener(listener) {
    changeListeners = changeListeners.filter((element) => {
      return element !== listener;
    });
  }

}

export default new TextStore();