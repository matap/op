# Monastery Uherský Brod

## Application setup

1. Install the latest version of NodeJS and npm. You can download both at <https://nodejs.org/en/download/>.
2. Clone the sample application repository.
3. Navigate to the root folder of the application in the command line.
4. Type `npm install` to install required npm packages.
5. Type `npm start` to start a development server.
6. The application opens in your browser at <http://localhost:3000>.

## Application build

1. Install the latest version of NodeJS and npm. You can download both at <https://nodejs.org/en/download/>.
2. Type `npm run build` to start building of web site
3. See folder /build

After starting, the sample application retrieves content from the default Kentico Cloud sample project.

## Content administration

Manage content in the content administration interface in <https://app.kenticocloud.com> 

You can learn more about content editing with Kentico Cloud in the [documentation](http://help.kenticocloud.com/).


## Created by

Thanks to Kentico to react sample app (<https://github.com/Kentico/cloud-sample-app-react>).

Created by Stanislav Kadlčík (video@matap.cz)